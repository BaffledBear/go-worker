PROJECT_NAME := "go-worker"
PKG := "gitlab.com/baffledbear/$(PROJECT_NAME)"
PKG_LIST := $(shell go list ./...)

build-for-test:
	docker build -t test -f Dockerfile.test .

lint: build-for-test
	@echo "====================Lint===================="
	@docker run test golint -set_exit_status ${PKG_LIST}
	@echo "Linting Complete"

test: build-for-test
	@echo "====================Test===================="
	@docker run test go test -short ${PKG_LIST}
	@echo "Unit Testing Complete"

race: build-for-test
	@echo "====================Race===================="
	@docker run test go test -short ${PKG_LIST}
	@echo "Data Race Complete"

msan: build-for-test
	@echo "==================Mem Scan=================="
	@docker run test go test -short ${PKG_LIST}
	@echo "Memory Scan Complete"

cover: build-for-test
	@echo "==================Coverage=================="
	@docker run test /bin/bash -c "go test -coverprofile=coverage.out ${PKG_LIST} && go tool cover -func=coverage.out"
	@echo "Coverage Complete"

build:
	@docker build -t ${PROJECT_NAME} .