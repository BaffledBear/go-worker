package datastore

import (
	"fmt"
	"os"
	"testing"
)

func TestContextConstructors(t *testing.T) {
	// Given
	host := "testhost"
	user := "testuser"
	pwd := "testpass"
	schema := "testdb"
	url := fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable", user, pwd, host, 5432, schema)

	// When
	_ = os.Setenv("DATABASE_HOST", host)
	_ = os.Setenv("DATABASE_USERNAME", user)
	_ = os.Setenv("DATABASE_PASSWORD", pwd)
	_ = os.Setenv("DATABASE_SCHEMA", schema)

	t.Run("TestMigrationContext", func(t *testing.T) {
		// When
		got := newDatabaseMigrationContext()

		// Then
		if !(got.Host == host) {t.Errorf("newDatabaseMigrationContest().Host = %s; want %s", got.Host, host)}
		if !(got.Username == user) {t.Errorf("newDatabaseMigrationContest().Username = %s; want %s", got.Username, user)}
		if !(got.Pass == pwd) {t.Errorf("newDatabaseMigrationContest().Pass = %s; want %s", got.Pass, pwd)}
		if !(got.Schema == schema) {t.Errorf("newDatabaseMigrationContest().Schema = %s; want %s", got.Schema, schema)}
		if !(got.Port == 5432) {t.Errorf("newDatabaseMigrationContest().Port = %d; want %d", got.Port, 5432)}
		if !(got.URL() == url) {t.Errorf("constructDatabaseWriterContext().URL = %s; want %s", got.URL(), url)}
	})

	t.Run("TestWriterContext", func(t *testing.T) {
		// When
		got := constructDatabaseWriterContext()

		// Then
		if !(got.Host == host) {t.Errorf("constructDatabaseWriterContext().Host = %s; want %s", got.Host, host)}
		if !(got.Username == user) {t.Errorf("constructDatabaseWriterContext().Username = %s; want %s", got.Username, user)}
		if !(got.Pass == pwd) {t.Errorf("constructDatabaseWriterContext().Pass = %s; want %s", got.Pass, pwd)}
		if !(got.Schema == schema) {t.Errorf("constructDatabaseWriterContext().Schema = %s; want %s", got.Schema, schema)}
		if !(got.Port == 5432) {t.Errorf("constructDatabaseWriterContext().Port = %d; want %d", got.Port, 5432)}
		if !(got.URL() == url) {t.Errorf("constructDatabaseWriterContext().URL = %s; want %s", got.URL(), url)}
	})
	t.Run("TestReaderContext", func(t *testing.T) {
		// When
		got := constructDatabaseReaderContext()

		// Then
		if !(got.Host == host) {t.Errorf("constructDatabaseReaderContext().Host = %s; want %s", got.Host, host)}
		if !(got.Username == user) {t.Errorf("constructDatabaseReaderContext().Username = %s; want %s", got.Username, user)}
		if !(got.Pass == pwd) {t.Errorf("constructDatabaseReaderContext().Pass = %s; want %s", got.Pass, pwd)}
		if !(got.Schema == schema) {t.Errorf("constructDatabaseReaderContext().Schema = %s; want %s", got.Schema, schema)}
		if !(got.Port == 5432) {t.Errorf("constructDatabaseReaderContext().Port = %d; want %d", got.Port, 5432)}
		if !(got.URL() == url) {t.Errorf("constructDatabaseReaderContext().URL = %s; want %s", got.URL(), url)}
	})

	// teardown
	_ = os.Setenv("DATABASE_HOST", "")
	_ = os.Setenv("DATABASE_USERNAME", "")
	_ = os.Setenv("DATABASE_PASSWORD", "")
	_ = os.Setenv("DATABASE_SCHEMA", "")
}
