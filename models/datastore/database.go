package datastore

import (
	"database/sql"
	"github.com/golang-migrate/migrate/v4"
	_ "github.com/lib/pq" // This helps register to datastore/sql
	"log"
	"time"
)

// Datastore provides an interface that can be used to Mock a DB
type Datastore interface {
}

// DB represents a datastore connection
type DB struct {
	*sql.DB
}

// Writer is the connection to write to the datastore
var Writer Datastore

// Reader is the connection to read from the datastore
var Reader Datastore

// NewDB returns a datastore connection using the provided context
func NewDB(ctx *dbContext) (*DB, error) {
	db, err := sql.Open("postgres", ctx.URL())
	if err != nil {
		return nil, err
	}
	if err = db.Ping(); err != nil {
		return nil, err
	}
	return &DB{db}, nil
}

// ConstructWriter builds the writer connection and sets Writer
func ConstructWriter() {
	context := constructDatabaseWriterContext()
	conn, err := NewDB(context)
	if err != nil {
		log.Fatal(err)
	}
	Writer = conn
}

// ConstructReader builds the reader connection and sets Reader
func ConstructReader() {
	context := constructDatabaseReaderContext()
	conn, err := NewDB(context)
	if err != nil {
		log.Fatal(err)
	}
	Reader = conn
}

// MigrateDb will perform a datastore migration
func MigrateDb() {
	log.Println("Performing Database Migration...")
	dbContext := newDatabaseMigrationContext()
	url := dbContext.URL()
	m, err := migrate.New(
		"file://models/migrations",
		url)
	if err != nil {
		log.Println("Had error, waiting 20 seconds and trying again.")
		time.Sleep(time.Second * 20) // wait for models to start and try again
		m, err = migrate.New(
			"file://models/migrations",
			url)
	}
	if err != nil {
		log.Fatal(err)
	}
	if err := m.Up(); err != nil && err != migrate.ErrNoChange {
		log.Fatal(err)
	}
	log.Println("Migration complete")
}

