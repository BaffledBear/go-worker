package datastore

import (
	"database/sql"
	"fmt"
	"gitlab.com/baffledbear/go-worker/context"
	"log"
	"strconv"
)

// dbContext holds information about a datastore connection
type dbContext struct {
	Host     string
	Port     int
	Username string
	Pass     string
	Schema   string
}

// newDatabaseMigrationContext constructs a new dbContext and returns it
func newDatabaseMigrationContext() *dbContext {
	host := context.GetEnv("DATABASE_HOST", "localhost")
	schema := context.GetEnv("DATABASE_SCHEMA", "devdb")
	user := context.GetEnv("DATABASE_USERNAME", "devuser")
	pass := context.GetEnv("DATABASE_PASSWORD", "mysecretpassword")
	port, _ := strconv.Atoi(context.GetEnv("DATABASE_PORT", "5432"))
	return &dbContext{
		Host:     host,
		Port:     port,
		Username: user,
		Pass:     pass,
		Schema:   schema,
	}
}

// constructDatabaseWriterContext constructs a new dbContext and returns it
func constructDatabaseWriterContext() *dbContext {
	host := context.GetEnv("DATABASE_HOST", "localhost")
	schema := context.GetEnv("DATABASE_SCHEMA", "devdb")
	user := context.GetEnv("DATABASE_USERNAME", "devuser")
	pass := context.GetEnv("DATABASE_PASSWORD", "mysecretpassword")
	port, _ := strconv.Atoi(context.GetEnv("DATABASE_PORT", "5432"))
	return &dbContext{
		Host:     host,
		Port:     port,
		Username: user,
		Pass:     pass,
		Schema:   schema,
	}
}

// constructDatabaseReaderContext constructs a new dbContext and returns it
func constructDatabaseReaderContext() *dbContext {
	host := context.GetEnv("DATABASE_HOST", "localhost")
	schema := context.GetEnv("DATABASE_SCHEMA", "devdb")
	user := context.GetEnv("DATABASE_USERNAME", "devuser")
	pass := context.GetEnv("DATABASE_PASSWORD", "mysecretpassword")
	port, _ := strconv.Atoi(context.GetEnv("DATABASE_PORT", "5432"))
	return &dbContext{
		Host:     host,
		Port:     port,
		Username: user,
		Pass:     pass,
		Schema:   schema,
	}
}

// URL returns a formatted url string for dc
func (dc *dbContext) URL() string {
	return fmt.Sprintf(
		"postgres://%s:%s@%s:%d/%s?sslmode=disable",
		dc.Username,
		dc.Pass,
		dc.Host,
		dc.Port,
		dc.Schema,
	)
}

func (dc *dbContext) OpenConnection() *sql.DB {
	conn, err := sql.Open("postgres", dc.URL())
	if err != nil {
		log.Fatal(err)
	}
	return conn
}

