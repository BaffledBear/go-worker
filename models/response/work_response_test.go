package response

import "testing"

func TestNewRegistrationResponse(t *testing.T) {
	got := NewWorkResponse("test message")
	if !(got.Message == "test message") {
		t.Errorf("NewWorkResponse(\"test message\").Message = %s; want \"test message\"", got.Message)
	}
}