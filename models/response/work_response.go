package response

// WorkResponse represents the response model for a registration attempt
type WorkResponse struct {
	Message string `json:"message" example:"Success" format:"string"`
}

// NewWorkResponse constructs a WorkResponse and returns it
func NewWorkResponse(message string) *WorkResponse {
	return &WorkResponse{Message: message}
}
