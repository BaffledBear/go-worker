CREATE TABLE IF NOT EXISTS hello(
    id serial PRIMARY KEY
    , hello VARCHAR (50) DEFAULT 'Hello'
    , world VARCHAR (50) DEFAULT 'World'
);