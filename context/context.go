package context

import (
	"os"
)

// Context is a base app context
type Context struct {
	HostURL string
}

// NewContext constructs a new Context and returns it
func NewContext() *Context {
	url := GetEnv("HOST_URL", "localhost")

	return &Context{HostURL: url}
}

// GetEnv gets the requested environment variable and returns it or the fallback string
func GetEnv(key, fallback string) string {
	value := os.Getenv(key)
	if len(value) == 0 {
		return fallback
	}
	return value
}
