FROM golang:alpine AS builder

RUN apk update && apk add --no-cache git

WORKDIR $GOPATH/src/gitlab.com/go-worker/

COPY . .
RUN go mod download && \
    go get -d -v &&    \
    go get -u github.com/swaggo/swag/cmd/swag &&  \
    swag init && \
    CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o /go/bin/app

################################
FROM scratch

EXPOSE 3000

COPY --from=builder /go/bin/app /go/bin/app
COPY --from=builder /go/src/gitlab.com/go-worker/views ./views
COPY --from=builder /go/src/gitlab.com/go-worker/models/migrations  ./models/migrations
COPY --from=builder /go/src/gitlab.com/go-worker/docs ./docs


ENTRYPOINT ["/go/bin/app"]