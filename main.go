package main

import (
	"github.com/gin-gonic/gin"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"gitlab.com/baffledbear/go-worker/models/datastore"
	"net/http"
)

func main() {
	datastore.MigrateDb()

	datastore.ConstructWriter()
	datastore.ConstructReader()
	// Set the router as the default one shipped with Gin
	router := gin.Default()
	router.Use(gin.Logger())

	// Setup route group for the API
	v1 := router.Group("/api/v1")
	{
		v1.GET("/", func(c *gin.Context) {
			c.JSON(http.StatusOK, gin.H{
				"message": "pong",
			})
		})
	}

	admin := router.Group("/admin")
	{
		admin.GET("/health", func(c *gin.Context) {
			c.JSON(http.StatusOK, gin.H{
				"message": "ok",
			})
		})

		admin.GET("/ready", func(c *gin.Context) {
			c.JSON(http.StatusOK, gin.H{
				"message": "ok",
			})
		})
	}
	router.Run(":3000")
}
