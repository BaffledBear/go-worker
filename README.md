# Go Worker

This is a template for a project that uses a combination of Go and Gin to process work on demand. On gitlab, a gitlab 
runner should be configured to use Docker-in-Docker to utilize this CI configuration.

https://docs.gitlab.com/ee/ci/docker/using_docker_build.html

`Makefile` exists as a method of locally testing a variety of things including starting `unit-tests`, `coverage`, `linting` and more.

