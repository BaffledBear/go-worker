module gitlab.com/baffledbear/go-worker

go 1.14

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/gin-gonic/gin v1.6.2
	github.com/go-openapi/spec v0.19.7 // indirect
	github.com/go-openapi/swag v0.19.8 // indirect
	github.com/golang-migrate/migrate/v4 v4.10.0
	github.com/lib/pq v1.3.0
	github.com/mailru/easyjson v0.7.1 // indirect
	github.com/urfave/cli v1.22.4 // indirect
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e // indirect
	golang.org/x/tools v0.0.0-20200410194907-79a7a3126eef // indirect
)
